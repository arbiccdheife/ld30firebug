package framework;

import org.newdawn.slick.GameContainer;

import entities.GameObject;

public class Camera {
	private final float TWEEN = .025f;
	
	private float x, y;
	private int xmin, xmax;
	
	
	public Camera(float x, float y, int levelWidth, GameContainer gc) {
		this.x = x;
		this.y = y;
		
		xmin = gc.getWidth() - levelWidth;
		xmax = 0;
	}
	
	
	public void update(GameObject player, GameContainer gc) {
		x += ((-player.getBounds().getX()+gc.getWidth()/2)-x) * TWEEN;
		if (x < xmin) x = xmin;
		if (x > xmax) x = xmax;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}

}
