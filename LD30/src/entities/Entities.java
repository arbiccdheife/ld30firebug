package entities;

public enum Entities {
	Player(),
	Block(),
	SmallEnemy(),
	MobBlocker(),
	FlameProjectile(),
	Scenery(),
	Powerup(),
	BottomlessPit(),
	LevelEnd(),
	ShooterEnemy(),
	BulletProjectile();

}
