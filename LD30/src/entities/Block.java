package entities;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

import framework.TileLoader;

public class Block extends GameObject {
	
	private Image tile;
	
	public Block(int x, int y, int type) {
		super();
		setBounds(x, y, 32, 32);
		setObjectId(Entities.Block);
		
		
		switch (type) {
		case 0 : //Grass block
			tile = TileLoader.getTile(1, 1);
			break;
		case 1 : //Plain block
			tile = TileLoader.getTile(1, 4);
			break;
		case 2 : //Left grass end plain block
			tile = TileLoader.getTile(1, 6);
			break;
		case 3 : //Right grass end plain block
			tile = TileLoader.getTile(1, 7);
			break;
		case 4 : //Left end platform
			tile = TileLoader.getTile(1, 8);
			break;
		case 5 : //Middle platform
			tile = TileLoader.getTile(1, 9);
			break;
		case 6 : //Right end platform
			tile = TileLoader.getTile(1, 10);
			break;
			
		}
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		//g.setColor(Color.gray);
		//g.draw(bounds);
		tile.draw(bounds.getX(), bounds.getY());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del, ArrayList<GameObject> gameObjects) {
		
	}

}
