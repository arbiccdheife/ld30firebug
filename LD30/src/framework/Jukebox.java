package framework;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class Jukebox {
	public static Sound changeSelection = null;
	public static Sound pickSelection = null;
	public static Sound jump = null;
	public static Sound levelEnd = null;
	public static Sound playerHit = null;
	public static Sound fallingDeath = null;
	public static Sound powerupPickup = null;
	public static Sound flamethrower = null;
	public static Sound scream1 = null;
	public static Sound scream2 = null;
	public static Sound scream3 = null;
	public static Sound fireRoasting = null;
	
	public static int numberFires;

	public static void initSounds() {
		try {
			changeSelection = new Sound("changeSelection.wav");
			pickSelection = new Sound("pickSelection.wav");
			jump = new Sound("jump.wav");
			levelEnd = new Sound("levelEnd.wav");
			playerHit = new Sound("playerHit.wav");
			fallingDeath = new Sound("fallingDeath.wav");
			powerupPickup = new Sound("powerupPickup.wav");
			flamethrower = new Sound("flamethrower.wav");
			scream1 = new Sound("scream1.wav");
			scream2 = new Sound("scream2.wav");
			scream3 = new Sound("scream3.wav");
			fireRoasting = new Sound("fireRoasting.wav");

		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		numberFires = 0;
	}

	public static Sound getScream(int num) {
		switch (num) {
		case 0:
			return scream1;
		case 1:
			return scream2;
		case 2:
			return scream3;
		default:
			System.out.println("default scream");
			return scream1;
		}
	}

}
