package entities;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public abstract class GameObject {
	protected Rectangle bounds;
	protected float velX, velY;
	
	public boolean isDestroyed = false;
	
	private Entities ObjectId;
	
	protected void setObjectId(Entities id) {
		this.ObjectId = id;
	}
	
	public Entities getObjectId() {
		return ObjectId;
	}
	
	protected void setBounds(int x, int y, int width, int height) {
		bounds = new Rectangle(x, y, width, height);
	}
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	public abstract void render(GameContainer gc, StateBasedGame sbg, Graphics g);
	public abstract void update(GameContainer gc, StateBasedGame sbg, int del, ArrayList<GameObject> gameObjects);
	
	
	public float getVelX() {
		return velX;
	}

	protected void setVelX(float velX) {
		this.velX = velX;
	}

	public float getVelY() {
		return velY;
	}

	protected void setVelY(float velY) {
		this.velY = velY;
	}

}
