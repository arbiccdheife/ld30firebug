package gamestates;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeOutTransition;

import entities.Background;
import entities.GameObject;
import entities.Player;
import framework.Camera;
import framework.LD30;
import framework.LevelLoader;
import framework.TileLoader;

public class Level2State extends BasicGameState {

	private Player player;
	private int levelWidth;
	private Camera cam;
	
	private Rectangle fuelGaugeOutline, fuelRemainingGauge;
	private Image heartTexture;

	private FadeOutTransition f = null;

	private ArrayList<GameObject> objects;
	private Background bg1, bg2;
	
	
	
	
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		loadLevel(gc, sbg, 3);
		bg1 = new Background("res/background1.png", levelWidth);
		bg2 = new Background("res/background2.png", levelWidth);
		fuelGaugeOutline = new Rectangle(30, 50, 150, 30);
		fuelRemainingGauge = new Rectangle(30, 50, 150, 30);
		heartTexture = TileLoader.getPowerupSprite(1, 0);
	}

	private void loadLevel(GameContainer gc, StateBasedGame sbg, int numberOfLives) {
		objects = new ArrayList<GameObject>();

		LevelLoader ll = new LevelLoader();
		player = ll.loadLevel("res/testlevel2.map", objects);
		player.setNumberOfLives(((LD30)sbg).player_lives);
		player.setFuelPercentage(((LD30)sbg).player_fuel_percent);
		levelWidth = ll.getLevelWidth();
		cam = new Camera(0, 0, levelWidth, gc);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		bg2.render(gc, sbg, g);
		bg1.render(gc, sbg, g);
		

		g.translate(cam.getX(), cam.getY());

		for (GameObject obj : objects) {
			obj.render(gc, sbg, g);
		}
		player.render(gc, sbg, g);
		g.translate(-cam.getX(), -cam.getY());

		renderHud(gc, sbg, g);
	}
	
	private void renderHud(GameContainer gc, StateBasedGame sbg, Graphics g) {
		
		for (int i=0; i < player.getNumberOfLives(); i++) {
			heartTexture.draw(30+40*i, 15);
		}
		
		float currFuel = player.getFuelPercentage();
		if (currFuel >= .7) {
			g.setColor(Color.green);
		} else if (currFuel >= .3) {
			g.setColor(Color.orange);
		} else {
			g.setColor(Color.red);
		}
		g.draw(fuelGaugeOutline);
		fuelRemainingGauge.setWidth(fuelGaugeOutline.getWidth()*currFuel);
		g.fill(fuelRemainingGauge);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del)
			throws SlickException {
		
		player.update(gc, sbg, del, objects);
		cam.update(player, gc);
		bg2.update(gc, sbg, del, objects, cam);
		bg1.update(gc, sbg, del, objects, cam);
		
		if (player.isDead && player.getNumberOfLives() > 0) {
			if (f == null) {
				f = new FadeOutTransition();
				sbg.enterState(this.getID(), f, null);
			} else if (f.isComplete()) {
				loadLevel(gc, sbg, player.getNumberOfLives());
				f = null;
			}
		} else if (player.isDead) {
			//Game Over
			System.exit(1);
		}
		
		if (player.atLevelEnd) {
			System.out.println("at level end");
		}
		
		for (GameObject obj : objects) {
			if (obj.isDestroyed)
				obj.update(gc, sbg, del, objects);
		}

		for (int i = objects.size() - 1; i >= 0; i--) {
			GameObject obj = objects.get(i);
			if (obj.isDestroyed) {
				objects.remove(i);
				continue;
			}
			obj.update(gc, sbg, del, objects);
		}
	}

	@Override
	public int getID() {
		return 1;
	}

}
