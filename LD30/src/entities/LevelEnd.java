package entities;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import framework.TileLoader;

public class LevelEnd extends GameObject{
	
	private Image texture;
	
	public LevelEnd(int x, int y, int type) {
		super();
		
		switch (type) { 
		case 0: //Normal level end
			setBounds(x, y, 32, 32);
			texture = TileLoader.getTile(1, 14);
			break;
		case 1: //Enemy portal...end of game
			setBounds(x,y, 64, 128);
			try {
				texture = new Image("portal.png");
			} catch (SlickException e) {
				e.printStackTrace();
			}
			break;
		
		}
		
		setObjectId(Entities.LevelEnd);
		
		
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		texture.draw(bounds.getX(), bounds.getY());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {
		
	}
	

}
