package entities;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.StateBasedGame;

import framework.Jukebox;
import framework.Particles;

public class SmallEnemy extends GameObject {
	private float movementSpeed = 3f;

	private float gravity = .1f;
	private int max_velocity = 10;

	private boolean facingRight;
	public boolean isDead;
	private int updatesSinceDead;

	private Animation walkingRight, walkingLeft;
	private Image deadLeft, deadRight;

	private ParticleSystem fire = null;
	
	private int deathScream, int numberOfScreamsPlayed;

	public SmallEnemy(int x, int y) {
		super();
		setBounds(x, y, 32, 32);
		setObjectId(Entities.SmallEnemy);
		facingRight = true;
		isDead = false;
		updatesSinceDead = 0;
		
		deathScream = (int) (Math.random() * 3);

		try {
			SpriteSheet ss = new SpriteSheet("enemySprites.png", 32, 33);
			Image[] walkLeftFrames = new Image[5];
			walkLeftFrames[0] = ss.getSprite(0, 0);
			walkLeftFrames[1] = ss.getSprite(1, 0);
			walkLeftFrames[2] = ss.getSprite(2, 0);
			walkLeftFrames[3] = ss.getSprite(3, 0);
			walkLeftFrames[4] = ss.getSprite(4, 0);

			Image[] walkRightFrames = new Image[5];
			walkRightFrames[0] = walkLeftFrames[0].getFlippedCopy(true, false);
			walkRightFrames[1] = walkLeftFrames[1].getFlippedCopy(true, false);
			walkRightFrames[2] = walkLeftFrames[2].getFlippedCopy(true, false);
			walkRightFrames[3] = walkLeftFrames[3].getFlippedCopy(true, false);
			walkRightFrames[4] = walkLeftFrames[4].getFlippedCopy(true, false);

			walkingRight = new Animation(walkRightFrames, 1000);
			walkingLeft = new Animation(walkLeftFrames, 1000);

			deadLeft = ss.getSprite(9, 0);
			deadRight = deadLeft.getFlippedCopy(true, false);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		if (facingRight) {
			if (!isDead) {
				walkingRight.draw(bounds.getX(), bounds.getY());
			} else {
				deadRight.draw(bounds.getX(), bounds.getY());
				if (fire != null)
					fire.render();
			}
		} else {
			if (!isDead) {
				walkingLeft.draw(bounds.getX(), bounds.getY());
			} else {
				deadLeft.draw(bounds.getX(), bounds.getY());
				if (fire != null)
					fire.render();
			}

		}
		// g.setColor(Color.red);
		// g.draw(bounds);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {

		if (isDead) {
			if (!Jukebox.getScream(deathScream).playing()) {
				Jukebox.getScream(deathScream).play();
			}
			if (fire == null) {
				fire = Particles.getFire(bounds.getX(), bounds.getY());
			}
			updatesSinceDead++;
			if (updatesSinceDead > 300) {
				if (Jukebox.getScream(deathScream).playing()) {
					Jukebox.getScream(deathScream).stop();
				}
				isDestroyed = true;
			}
			if (fire != null) {
				fire.update(del);
				fire.setPosition(bounds.getX()+16, bounds.getY()+23);
			}
		}

		if (!isDead) {
			walkingRight.update(del);
			walkingLeft.update(del);
			if (facingRight) {
				velX = movementSpeed;
			} else {
				velX = -movementSpeed;
			}
		} else {
			velX = 0;
		}

		velY += gravity * del;
		if (velY > max_velocity)
			velY = max_velocity;

		bounds.setX(bounds.getX() + velX);
		bounds.setY(bounds.getY() + velY);
		// System.out.println(velY);

		Rectangle sideBounds = getSideBounds();
		Rectangle lowerBound = getLowerBound();

		for (GameObject obj : gameObjects) {
			Entities tmpId = obj.getObjectId();

			if (tmpId == Entities.Block || tmpId == Entities.MobBlocker) {
				Rectangle tmp = obj.getBounds();
				if (sideBounds.intersects(tmp)) {
					facingRight = !facingRight;
					velX = -velX;
				}

				if (lowerBound.intersects(tmp)) {
					velY = 0;
					bounds.setY(tmp.getY() - bounds.getHeight() - 1);
				}
			}
		}

	}

	private Rectangle getSideBounds() {
		float x = bounds.getX();
		float y = bounds.getY() + 12;
		return new Rectangle(x, y, 32, 8);
	}

	private Rectangle getLowerBound() {
		float x = bounds.getX() + 8;
		float y = bounds.getY() + 24;
		return new Rectangle(x, y, 16, 9);
	}

}
