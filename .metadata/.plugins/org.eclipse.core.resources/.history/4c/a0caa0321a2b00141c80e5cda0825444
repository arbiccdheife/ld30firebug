package entities;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class Player extends GameObject {
	private final float FUEL_DEPLETION_RATE = .007f;

	private float movementSpeed = 5;

	private float gravity = .5f;
	private int max_velocity = 10;
	
	private float fuel_percent;
	private int number_of_lives;

	boolean isFalling, isJumping;
	private boolean facingRight;

	private FlameProjectile flame;

	SpriteSheet ss;

	Animation idleRight, idleLeft;
	Animation walkRight, walkLeft;
	Image idleFiringRight, idleFiringLeft;

	public Player(int x, int y) {
		super();
		setBounds(x, y, 32, 64);
		setObjectId(Entities.Player);
		isFalling = true;
		isJumping = true;
		facingRight = true;
		
		fuel_percent= 1f;

		flame = new FlameProjectile(0, 0, false);

		try {
			ss = new SpriteSheet("res/playerSprites.png", 32, 64);
		} catch (SlickException e) {
			e.printStackTrace();
		}

		// idle = ss.getSprite(0, 0);

		Image[] idleRightFrames = new Image[3];
		idleRightFrames[0] = ss.getSprite(0, 0);
		idleRightFrames[1] = ss.getSprite(1, 0);
		idleRightFrames[2] = ss.getSprite(2, 0);

		Image[] idleLeftFrames = new Image[3];
		idleLeftFrames[0] = idleRightFrames[0].getFlippedCopy(true, false);
		idleLeftFrames[1] = idleRightFrames[1].getFlippedCopy(true, false);
		idleLeftFrames[2] = idleRightFrames[2].getFlippedCopy(true, false);

		idleRight = new Animation(idleRightFrames, 2500);
		idleLeft = new Animation(idleLeftFrames, 2500);

		Image[] walkRightFrames = new Image[4];
		walkRightFrames[0] = ss.getSprite(1, 1);
		walkRightFrames[1] = ss.getSprite(2, 1);
		walkRightFrames[2] = ss.getSprite(3, 1);
		walkRightFrames[3] = ss.getSprite(4, 1);

		Image[] walkLeftFrames = new Image[4];
		walkLeftFrames[0] = walkRightFrames[0].getFlippedCopy(true, false);
		walkLeftFrames[1] = walkRightFrames[1].getFlippedCopy(true, false);
		walkLeftFrames[2] = walkRightFrames[2].getFlippedCopy(true, false);
		walkLeftFrames[3] = walkRightFrames[3].getFlippedCopy(true, false);

		walkRight = new Animation(walkRightFrames, 750);
		walkLeft = new Animation(walkLeftFrames, 750);

		idleFiringRight = ss.getSprite(0, 1);
		idleFiringLeft = idleFiringRight.getFlippedCopy(true, false);

	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		if (facingRight && velX == 0) {
			if (flame.isActive()) {
				idleFiringRight.draw(bounds.getX(), bounds.getY());
			} else {
				idleRight.draw(bounds.getX(), bounds.getY());
			}
		} else if (facingRight) {
			walkRight.draw(bounds.getX(), bounds.getY());
		} else if (velX != 0) {
			walkLeft.draw(bounds.getX(), bounds.getY());
		} else {
			if (flame.isActive()) {
				idleFiringLeft.draw(bounds.getX(), bounds.getY());
			} else {
				idleLeft.draw(bounds.getX(), bounds.getY());
			}
		}

		flame.render(gc, sbg, g);
//		g.setColor(Color.red);
//		g.drawString(Float.toString(fuel_percent), 50, 200);
		// g.draw(getLowerBounds());
		// g.draw(getUpperBounds());
		// g.draw(getLeftBounds());
		// g.draw(getRightBounds());
	}

	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {
		idleRight.update(del);
		idleLeft.update(del);
		walkRight.update(del);
		walkLeft.update(del);

		velY += gravity * del;
		if (velY > max_velocity)
			velY = max_velocity;

		Input in = gc.getInput();

		if (in.isKeyDown(Input.KEY_D)) {
			facingRight = true;
			velX = movementSpeed;
		} else if (in.isKeyDown(Input.KEY_A)) {
			facingRight = false;
			velX = -movementSpeed;
		} else {
			velX = 0;
		}

		if (in.isKeyDown(Input.KEY_F) && fuel_percent > 0) {
			if (facingRight) {
				flame.setX((int) bounds.getX() + 34);
				flame.setY((int) bounds.getY() + 27);
			} else {
				flame.setX((int) (bounds.getX() - flame.getBounds().getWidth()));
				flame.setY((int) bounds.getY() + 27);
			}
			flame.setIsActive(true);
			fuel_percent -= FUEL_DEPLETION_RATE;
		} else {
			flame.setIsActive(false);
		}
		
		if (fuel_percent < 0) fuel_percent = 0;

		if (!isJumping && in.isKeyPressed(Input.KEY_SPACE)) {
			isJumping = true;
			velY = -50f;
		}

		bounds.setX(bounds.getX() + velX);
		bounds.setY(bounds.getY() + velY);

		collision(gameObjects);
		flame.update(gc, sbg, del, gameObjects);

	}

	private void collision(ArrayList<GameObject> gameObjects) {
		boolean isGrounded = false;
		Rectangle lower = getLowerBounds();
		Rectangle upper = getUpperBounds();
		Rectangle left = getLeftBounds();
		Rectangle right = getRightBounds();

		for (GameObject obj : gameObjects) {
			Entities tmpId = obj.getObjectId();
			if (tmpId == Entities.Block) {
				Rectangle tmp = obj.getBounds();
				if (lower.intersects(tmp)) {
					isGrounded = true;
					isFalling = false;
					isJumping = false;
					velY = 0;
					bounds.setY(tmp.getY() - 64);
				}
				if (upper.intersects(tmp) || tmp.contains(upper)) {
					velY = 0;
					bounds.setY(tmp.getY() + tmp.getHeight() + 1);
				} else if (right.intersects(tmp)) {
					bounds.setX(tmp.getX() - bounds.getWidth() - 2);
				} else if (left.intersects(tmp) || tmp.contains(left)) {
					bounds.setX(tmp.getX() + tmp.getWidth() + 1);
				}
			} else if (tmpId == Entities.Powerup) {
				if (bounds.intersects(obj.getBounds())) {
					int powerupType = ((Powerup)obj).getType();
					obj.isDestroyed = true;
					switch (powerupType) {
					case 0:
						fuel_percent += .5f;
						if (fuel_percent > 1)
							fuel_percent = 1;
						break;
					}
				}
			}
			
			
			
		}

		isFalling = !isGrounded;
	}
	
	public float getFuelPercentage() {
		return fuel_percent;
	}
	
	public int getNumberOfLives() {
		return number_of_lives;
	}
	
	public void setNumberOfLives(int num) {
		number_of_lives = num;
	}

	private Rectangle getLowerBounds() {
		return new Rectangle(bounds.getCenterX() - 9, bounds.getY() + 54, 20,
				11);

	}

	private Rectangle getUpperBounds() {
		return new Rectangle(bounds.getCenterX() - 8, bounds.getY(), 20, 10);
	}

	private Rectangle getRightBounds() {
		float x = bounds.getX() + bounds.getWidth() - 10;
		float y = bounds.getY() + 10;
		return new Rectangle(x, y, 10, bounds.getHeight() - 30);
	}

	private Rectangle getLeftBounds() {
		float x = bounds.getX() + 1;
		float y = bounds.getY() + 10;
		return new Rectangle(x, y, 10, bounds.getHeight() - 30);
	}
}
