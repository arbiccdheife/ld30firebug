package entities;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.StateBasedGame;

import framework.Particles;

public class ShooterEnemy extends GameObject {
	private final int TIME_BETWEEN_SHOTS = 300;

	private float movementSpeed = 1f;

	private float gravity = .1f;
	private int max_velocity = 10;

	private boolean facingRight;
	public boolean isDead, isShooting;
	private int updatesSinceDead, updatesSinceShot, updatesShooting;

	private Animation walkingRight, walkingLeft;
	private Image shootLeft, shootRight;
	private Image deadLeft, deadRight;

	private ParticleSystem fire = null;

	public ShooterEnemy(int x, int y) {
		super();
		setBounds(x, y, 32, 64);
		setObjectId(Entities.ShooterEnemy);
		facingRight = true;
		isDead = false;
		isShooting = false;
		updatesSinceDead = 0;
		updatesSinceShot = 0;

		try {
			SpriteSheet ss = new SpriteSheet("res/shooterSheet.png", 32, 65);
			Image[] walkRightFrames = new Image[5];
			walkRightFrames[0] = ss.getSprite(0, 0);
			walkRightFrames[1] = ss.getSprite(1, 0);
			walkRightFrames[2] = ss.getSprite(2, 0);
			walkRightFrames[3] = ss.getSprite(3, 0);
			walkRightFrames[4] = ss.getSprite(4, 0);

			Image[] walkLeftFrames = new Image[5];
			walkLeftFrames[0] = walkRightFrames[0].getFlippedCopy(true, false);
			walkLeftFrames[1] = walkRightFrames[1].getFlippedCopy(true, false);
			walkLeftFrames[2] = walkRightFrames[2].getFlippedCopy(true, false);
			walkLeftFrames[3] = walkRightFrames[3].getFlippedCopy(true, false);
			walkLeftFrames[4] = walkRightFrames[4].getFlippedCopy(true, false);

			walkingRight = new Animation(walkRightFrames, 500);
			walkingLeft = new Animation(walkLeftFrames, 500);

			shootRight = ss.getSprite(8, 0);
			shootLeft = shootRight.getFlippedCopy(true, false);
			//
			deadRight = ss.getSprite(9, 0);
			deadLeft = deadRight.getFlippedCopy(true, false);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {

		if (facingRight) {
			if (isShooting && !isDead) {
				shootRight.draw(bounds.getX(), bounds.getY());
			} else if (!isDead) {
				walkingRight.draw(bounds.getX(), bounds.getY());
			} else {
				deadRight.draw(bounds.getX(), bounds.getY());
				if (fire != null)
					fire.render();
			}
		} else {
			if (isShooting) {
				shootLeft.draw(bounds.getX(), bounds.getY());
			} else if (!isDead) {
				walkingLeft.draw(bounds.getX(), bounds.getY());
			} else {
				deadLeft.draw(bounds.getX(), bounds.getY());
				if (fire != null)
					fire.render();
			}

		}
		// g.setColor(Color.red);
		// g.draw(bounds);
		// g.draw(getLowerBound());
		// g.draw(getSideBounds());
	}

	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects, Player player) {
		if (isDead) {
			if (fire == null) {
				fire = Particles.getFire(bounds.getX(), bounds.getY());
			}
			updatesSinceDead++;
			if (updatesSinceDead > 300) {
				isDestroyed = true;
			}
			if (fire != null) {
				fire.update(del);
				fire.setPosition(bounds.getX() + 16, bounds.getY() + 60);
			}
		}

		if (!isDead) {
			updatesSinceShot++;

			walkingRight.update(del);
			walkingLeft.update(del);
			if (isShooting) {
				updatesShooting++;
				velX = 0;
				if (updatesShooting > 50) {
					if (facingRight) {
						gameObjects.add(new BulletProjectile((int) bounds
								.getX() + 31, (int) bounds.getY() + 15, true));
					} else {
						gameObjects.add(new BulletProjectile((int) bounds
								.getX() + 1, (int) bounds.getY() + 15, false));
					}
					isShooting = false;
				}

			} else if (canShoot(player)) {
				isShooting = true;
				updatesSinceShot = 0;
				updatesShooting = 0;
			} else {
				if (facingRight) {
					velX = movementSpeed;
				} else {
					velX = -movementSpeed;
				}
			}
		} else {
			velX = 0;
		}

		velY += gravity * del;
		if (velY > max_velocity)
			velY = max_velocity;

		bounds.setX(bounds.getX() + velX);
		bounds.setY(bounds.getY() + velY);
		// System.out.println(velY);

		Rectangle sideBounds = getSideBounds();
		Rectangle lowerBound = getLowerBound();

		for (GameObject obj : gameObjects) {
			Entities tmpId = obj.getObjectId();

			if (tmpId == Entities.Block || tmpId == Entities.MobBlocker) {
				Rectangle tmp = obj.getBounds();
				if (sideBounds.intersects(tmp)) {
					facingRight = !facingRight;
					velX = -velX;
				}

				if (lowerBound.intersects(tmp)) {
					velY = 0;
					bounds.setY(tmp.getY() - bounds.getHeight() - 1);
				}
			}
		}
	}

	private boolean canShoot(Player player) {
		if (updatesSinceShot >= TIME_BETWEEN_SHOTS) {
			if (facingRight && player.getBounds().getX() > bounds.getX() + 10) {
				return true;
			} else if (!facingRight
					&& player.getBounds().getX() < bounds.getX() - 10) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {

	}

	private Rectangle getSideBounds() {
		float x = bounds.getX();
		float y = bounds.getY() + 12;
		return new Rectangle(x, y, 32, 30);
	}

	private Rectangle getLowerBound() {
		float x = bounds.getX() + 8;
		float y = bounds.getY() + 56;
		return new Rectangle(x, y, 16, 9);
	}

}
