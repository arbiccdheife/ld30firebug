package gamestates;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import entities.Background;
import entities.Entities;
import entities.GameObject;
import entities.Player;
import entities.ShooterEnemy;
import framework.Camera;
import framework.Jukebox;
import framework.LD30;
import framework.LevelLoader;
import framework.TileLoader;

public class Level3State extends BasicGameState {

	private Player player;
	private int levelWidth;
	private Camera cam;
	
	private Rectangle fuelGaugeOutline, fuelRemainingGauge;
	private Image heartTexture;

	private FadeOutTransition f = null;

	private ArrayList<GameObject> objects;
	private Background bg1, bg2;
	
	
	
	
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		loadLevel(gc, sbg, 3);
		
		fuelGaugeOutline = new Rectangle(30, 50, 150, 30);
		fuelRemainingGauge = new Rectangle(30, 50, 150, 30);
		heartTexture = TileLoader.getPowerupSprite(1, 0);
	}

	public void loadLevel(GameContainer gc, StateBasedGame sbg, int numberOfLives) {
		objects = new ArrayList<GameObject>();
		
		bg1 = new Background("background1.png", levelWidth);
		bg2 = new Background("background2.png", levelWidth);
		
		if (Jukebox.fireRoasting.playing()) {
			Jukebox.fireRoasting.stop();
		}
		Jukebox.numberFires = 0;


		LevelLoader ll = new LevelLoader();
		player = ll.loadLevel("/level3.map", objects);
		player.setNumberOfLives(numberOfLives);
		player.setFuelPercentage(((LD30)sbg).player_fuel_percent);
		levelWidth = ll.getLevelWidth();
		cam = new Camera(0, 0, levelWidth, gc);
	}
	
	public void loadPlayerStats(StateBasedGame sbg) {
		player.setNumberOfLives(((LD30)sbg).player_lives);
		player.setFuelPercentage(((LD30)sbg).player_fuel_percent);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		bg2.render(gc, sbg, g);
		bg1.render(gc, sbg, g);
		

		g.translate(cam.getX(), cam.getY());

		for (GameObject obj : objects) {
			obj.render(gc, sbg, g);
		}
		player.render(gc, sbg, g);
		g.translate(-cam.getX(), -cam.getY());

		renderHud(gc, sbg, g);
	}
	
	private void renderHud(GameContainer gc, StateBasedGame sbg, Graphics g) {
		
		for (int i=0; i < player.getNumberOfLives(); i++) {
			heartTexture.draw(30+40*i, 15);
		}
		
		float currFuel = player.getFuelPercentage();
		if (currFuel >= .7) {
			g.setColor(Color.green);
		} else if (currFuel >= .3) {
			g.setColor(Color.orange);
		} else {
			g.setColor(Color.red);
		}
		g.draw(fuelGaugeOutline);
		fuelRemainingGauge.setWidth(fuelGaugeOutline.getWidth()*currFuel);
		g.fill(fuelRemainingGauge);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del)
			throws SlickException {
		
		Input in = gc.getInput();
		if (in.isKeyPressed(Input.KEY_ESCAPE)) {
			((LD30)sbg).paused_level_id = getID();
			sbg.enterState(6, new FadeOutTransition(), new FadeInTransition());
		}
		
		player.update(gc, sbg, del, objects);
		cam.update(player, gc);
		bg2.update(gc, sbg, del, objects, cam);
		bg1.update(gc, sbg, del, objects, cam);
		
		if (player.isDead && player.getNumberOfLives() > 0) {
			if (f == null) {
				f = new FadeOutTransition();
				sbg.enterState(this.getID(), f, null);
			} else if (f.isComplete()) {
				loadLevel(gc, sbg, player.getNumberOfLives());
				f = null;
			}
		} else if (player.isDead) {
			//Game Over			
			((LD30)sbg).reloadLevels(gc, sbg);
			sbg.enterState(5, new FadeOutTransition(), new FadeInTransition());
		}
		
		if (player.atLevelEnd) {
			System.out.println("at level end");
		}
		

		for (int i = objects.size() - 1; i >= 0; i--) {
			GameObject obj = objects.get(i);
			if (obj.isDestroyed) {
				objects.remove(i);
				continue;
			}
			
			if (obj.getObjectId() == Entities.ShooterEnemy) {
				ShooterEnemy tmp = (ShooterEnemy)obj;
				tmp.update(gc, sbg, del, objects, player);
			} else {
				obj.update(gc, sbg, del, objects);
			}
			
		}
	}

	@Override
	public int getID() {
		return 2;
	}

}
