package entities;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.StateBasedGame;

public class BulletProjectile extends GameObject {
	private final float BULLET_SPEED = .3f;
	private final int LIFESPAN = 150; //updates
	
	private int updatesAlive;
	
	private Animation animation;
	
	private float dx;
	
	public BulletProjectile(int x, int y, boolean goingRight) {
		super();
		setBounds(x, y, 8, 8);
		setObjectId(Entities.BulletProjectile);
		updatesAlive = 0;
		if (goingRight) {
			dx = BULLET_SPEED;
		} else {
			dx = -BULLET_SPEED;
		}
		
		try {
			SpriteSheet ss = new SpriteSheet("acidSheet.png", 8, 8);
			Image[] acidFrames = new Image[4];
			acidFrames[0] = ss.getSprite(0, 0);
			acidFrames[1] = ss.getSprite(1, 0);
			acidFrames[2] = ss.getSprite(2, 0);
			acidFrames[3] = ss.getSprite(3, 0);
			
			animation = new Animation(acidFrames, 50);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		animation.draw(bounds.getX(), bounds.getY());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {
		updatesAlive++;
		if (updatesAlive >= LIFESPAN) {
			isDestroyed = true;
		}
		
		animation.update(del);
		bounds.setX(bounds.getX()+dx*del);
	}

}
