package entities;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

import framework.TileLoader;

public class Scenery extends GameObject {

	private Image texture;
	public boolean isFlammable;

	public Scenery(int x, int y, int type) {
		super();
		setBounds(x, y, 32, 32);
		setObjectId(Entities.Scenery);
		
		switch(type) {
		case 0: //Left edge grass
			isFlammable = false;
			texture = TileLoader.getTile(1, 0);
			break;
		case 1: //Right edge grass
			isFlammable = false;
			texture = TileLoader.getTile(1, 2);
			break;
		case 2: //Left edge plain
			isFlammable = false;
			texture = TileLoader.getTile(1, 3);
			break;
		case 3: //Right edge plain
			isFlammable = false;
			texture = TileLoader.getTile(1, 5);
			break;
			default:
				isDestroyed = true;
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		texture.draw(bounds.getX(), bounds.getY());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {

	}

}
