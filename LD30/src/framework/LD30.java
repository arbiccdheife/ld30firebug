package framework;

import gamestates.HelpState;
import gamestates.IntroState;
import gamestates.Level1State;
import gamestates.Level2State;
import gamestates.Level3State;
import gamestates.Level4State;
import gamestates.MainMenuState;
import gamestates.PauseState;
import gamestates.VictoryState;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.SoundStore;
import org.newdawn.slick.state.StateBasedGame;

public class LD30 extends StateBasedGame{
	
	static final int GAME_WIDTH = 800;
	static final int GAME_HEIGHT = 600;
	
	public float player_fuel_percent;
	public int player_lives;
	
	public int paused_level_id;
	
	
	Level1State l1;
	Level2State l2;
	Level3State l3;
	Level4State l4;

	public LD30( ) {
		super("FireBug");
		
		l1 = new Level1State();
		l2 = new Level2State();
		l3 = new Level3State();
		l4 = new Level4State();
		
		
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.addState(new MainMenuState());
		this.addState(new IntroState());
		this.addState(new HelpState());
		this.addState(new PauseState());
		this.addState(new VictoryState());
		this.addState(l1);
		this.addState(l2);
		this.addState(l3);
		this.addState(l4);
		
		Jukebox.initSounds();
		SoundStore.get().setSoundVolume(.7f);
		
		
	}
	
	public void reloadLevels(GameContainer gc, StateBasedGame sbg) {
		l1.loadLevel(gc, sbg, 3);
		l2.loadLevel(gc, sbg, 3);
		l3.loadLevel(gc, sbg, 3);
		l4.loadLevel(gc, sbg, 3);
	}
	
	public void setPlayerFuel(float val) {
		player_fuel_percent = val;
	}
	
	public void setPlayerLives(int val) {
		player_lives = val;
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer app = new AppGameContainer(new LD30());
			app.setDisplayMode(GAME_WIDTH, GAME_HEIGHT, false);
			app.setTargetFrameRate(60);
			app.setShowFPS(false);
			//app.setIcon("res/icon.png");
			app.setIcon("icon.png");
			app.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
