package framework;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class TileLoader {
	private static SpriteSheet tileSheet = null;
	private static SpriteSheet powerupSheet = null;
	
	
	public static Image getTile(int row, int col) {
		if (tileSheet == null) {
			try {
				tileSheet = new SpriteSheet("tiles.png",  32, 32);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	
		return tileSheet.getSprite(col, row);
	}
	
	public static Image getPowerupSprite(int row, int col) {
		if (powerupSheet == null) {
			try {
				powerupSheet = new SpriteSheet("powerupSprites.png", 32, 32);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		
		return powerupSheet.getSprite(col, row);
	}

}
