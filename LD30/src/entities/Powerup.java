package entities;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

import framework.TileLoader;

public class Powerup extends GameObject {

	private Image texture;
	private int type;

	public Powerup(int x, int y, int type) {
		super();
		setBounds(x, y, 32, 32);
		setObjectId(Entities.Powerup);
		this.type = type;
		
		switch (type) {
		case 0: // Fuel powerup
			texture = TileLoader.getPowerupSprite(0, 0);
			break;
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		texture.draw(bounds.getX(), bounds.getY());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int del,
			ArrayList<GameObject> gameObjects) {

	}
	
	public int getType() {
		return type;
	}

}
